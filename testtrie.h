#ifndef TESTTRIE_H
#define TESTTRIE_H

#include <QObject>


class TestTrie: public QObject
{
    Q_OBJECT
private slots:
    void emptyTrie();
    void oneChar();
    void lookup();
    void findAll();
    void findAll2();
public:
    TestTrie();
};

#endif // TESTDIRLISTER_H
