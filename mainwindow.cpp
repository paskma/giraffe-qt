#include "mainwindow.h"

#include <QDebug>
#include <QTime>
#include <QStringListModel>
#include "doc.h"
#include "index.h"


MainWindow::MainWindow(Model &model, QWidget *parent) :
    QWidget(parent), m_model(model)
{
    m_queryEdit = new QLineEdit;
    m_resultList = new QListView;
    m_resultList->setModel(new QStringListModel(m_resultData, this));
    m_layout = new QGridLayout;
    m_layout->addWidget(m_queryEdit, 0, 0);
    m_layout->addWidget(m_resultList, 1, 0);
    setLayout(m_layout);
    resize(1200, 800);

    connect(m_queryEdit, SIGNAL(textChanged(QString)), this, SLOT(queryChanged(QString)));
}

void MainWindow::queryChanged(QString query)
{
    qDebug() << query;
    QStringList words = query.split(' ', QString::SkipEmptyParts);
    QTime stopWatch;
    stopWatch.start();
    QList<Doc> result = m_model.getIndex().expansionQueryAnd(words);
    qDebug() << result.size() << "results in " << stopWatch.elapsed() << "ms.";

    m_resultData.clear();
    int docLimit = qMin(result.size(), 200);
    for (int i = 0; i < docLimit; i++)
    {
        m_resultData.append(result.at(i).getContent());
    }
    ((QStringListModel*)m_resultList->model())->setStringList(m_resultData);
}
