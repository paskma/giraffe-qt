#include "doc.h"

Doc::Doc(qint64 id, const QString & content) : m_id(id), m_content(content)
{
}

Doc::Doc()
{
}

const QString &Doc::getContent() const
{
    return m_content;
}

qint64 Doc::getId() const
{
    return m_id;
}

bool Doc::operator ==(Doc &doc)
{
    return getId() == doc.getId() && getContent() == doc.getContent();
}

const QRegExp re("[/ \\&\\+,\\-_\\.=\\[\\]\\(\\)\\\\*'\"]");

QStringList Doc::getWords() const
{
    QStringList words = getContent().split(re, QString::SkipEmptyParts);
    for (int i = 0; i < words.size(); i++)
        words[i] = normalizeWord(words[i]);

    return words;
}

QDataStream & operator<< (QDataStream& stream, const Doc& doc)
{
    stream << doc.m_id << doc.m_content;
    return stream;
}

QDataStream & operator>> (QDataStream& stream, Doc& doc)
{
    stream >> doc.m_id;
    stream >> doc.m_content;
    return stream;
}


QString Doc::normalizeWord(const QString& word)
{
    return word.toLower();
}
