CONFIG += qtestlib

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    testqstring.cpp \
    dirlister.cpp \
    testdirlister.cpp \
    doc.cpp \
    testindex.cpp \
    index.cpp \
    buildindex.cpp \
    model.cpp \
    trie.cpp

HEADERS += \
    mainwindow.h \
    testqstring.h \
    dirlister.h \
    testdirlister.h \
    doc.h \
    testindex.h \
    index.h \
    buildindex.h \
    model.h \
    trie.h
