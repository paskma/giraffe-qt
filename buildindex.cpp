#include "buildindex.h"

#include "index.h"
#include "dirlister.h"

#include <QDebug>
#include <QTime>
#include <iostream>

using std::endl;

BuildIndex::BuildIndex()
{
}

void BuildIndex::build(const QStringList &dirs, const QString &indexFile)
{
    Index index;

    for (auto dir : dirs)
    {
        qDebug() << "Listing" << dir;
        DirLister lister(dir);
        auto listing = lister.allFiles();

        //for (auto s : listing) qDebug() << s;
        QTime stopWatch;
        stopWatch.start();
        qDebug() << "Inverting" << listing.size() << "docs...";
        importToIndex(index, listing);
        qDebug() << "..." << stopWatch.elapsed() << "ms.";
    }

    QFile f(indexFile);
    qDebug() << "Saving...";
    index.save(f);
}
