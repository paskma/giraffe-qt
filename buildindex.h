#ifndef BUILDINDEX_H
#define BUILDINDEX_H

#include <QStringList>

class BuildIndex
{
public:
    BuildIndex();
    void build(const QStringList &dirs, const QString &indexFile);
};

#endif // BUILDINDEX_H
