#ifndef TRIE_H
#define TRIE_H

#include <string>
#include <map>
#include <set>
#include <iterator>
#include <vector>

template<typename K, typename V>
struct Node
{
    typedef typename std::iterator_traits<typename K::iterator>::value_type key_element_type;
    std::map<key_element_type, Node<K,V> > children;
    V value;

    Node(): value{} {}

    int count() const {
        int sum = 0;
        for (const auto& i : children) {
            sum += i.second.count();
        }
        return 1 + sum;
    }

    std::vector<const V*> enumerate(const V& exclude) const
    {
        std::vector<const V*> result;

        if (value != exclude){
            result.push_back(&value);
        }

        for (const auto& i : children) {
            const auto& deep = i.second.enumerate(exclude);
            result.insert(result.end(), deep.begin(), deep.end());
        }
        return result;
    }
};

template<typename K, typename V>
class Trie
{
private:
    Node<K, V> root;
public:
    Trie() {}
    V& add(const K& s);
    const V& find(const K& s) const;
    std::vector<const V*> findAll(const K& s) const;
    int nodeCount() const;
};


template<typename K, typename V>
V& Trie<K,V>::add(const K& s)
{
    Node<K,V> *node = &root;
    for(auto i = s.begin(); i != s.end(); ++i) {
        Node<K,V> &next = node->children[*i];
        if (i == s.end() - 1) {
            return next.value;
        } else {
            node = &next;
        }
    }

    return root.value;
}

template<typename K, typename V>
const V& Trie<K,V>::find(const K& s) const
{
    const Node<K,V> *node = &root;
    for(auto i = s.begin(); i != s.end(); ++i) {
        const auto next = node->children.find(*i);

        if (next == node->children.end()) {
            return root.value;
        } else if (i == s.end() - 1) {
            return next->second.value;
        } else {
            node = &next->second;
        }
    }

    return root.value;
}

template<typename K, typename V>
std::vector<const V*> Trie<K,V>::findAll(const K& s) const
{
    const Node<K,V> *node = &root;
    for(auto i = s.begin(); i != s.end(); ++i) {
        const auto next = node->children.find(*i);

        if (next == node->children.end()) {
            return std::vector<const V*>();
        } else if (i == s.end() - 1) {
            return next->second.enumerate(root.value);
        } else {
            node = &next->second;
        }
    }

    return std::vector<const V*>();
}


template<typename K, typename V>
int Trie<K,V>::nodeCount() const {
    return root.count();
}

#endif // TRIE_H





































