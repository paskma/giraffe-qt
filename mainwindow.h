#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QStringListModel>
#include <model.h>

class MainWindow : public QWidget
{
    Q_OBJECT
private:
    QGridLayout *m_layout;
    QLineEdit *m_queryEdit;
    QListView *m_resultList;
    QStringList m_resultData;
    Model &m_model;
public:
    explicit MainWindow(Model &model, QWidget *parent = 0);

signals:
    
public slots:
private slots:
    void queryChanged(QString query);
};

#endif // MAINWINDOW_H
