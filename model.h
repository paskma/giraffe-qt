#ifndef MODEL_H
#define MODEL_H

#include <index.h>
#include <QString>

class Model
{
private:
    Index m_index;
    QString m_indexLocation;
public:
    Model();
    Model& operator=(const Model&) = delete;

    bool init();
    void setIndexLocation(const QString &location);
    Index &getIndex();
};

#endif // MODEL_H
