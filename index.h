#ifndef INDEX_H
#define INDEX_H

#include <QLinkedList>
#include <QSet>
#include <QHash>
#include <QVector>
#include <QString>
#include <QFile>
#include "doc.h"
#include "trie.h"

class IndexException: public std::exception
{
private:
    std::string msg;
public:
    IndexException(const std::string& msg);
    IndexException(const QString& msg);
    virtual const char * what() const noexcept;
};

class Index
{
private:
    QList<Doc> m_docs;
    QHash<QString, QSet<qint64>> m_wordsToDocs;
    qint64 m_lastUsedDocId = -1; // also doc pos
    Trie<QString, QString> m_wordTrie;
    void addDocToIndex(const Doc& doc);
    QSet<qint64> wordExpansion(const QString& word) const;
    friend QDataStream & operator<< (QDataStream& stream, const Index& index);
    friend QDataStream & operator>> (QDataStream& stream, Index& index);
public:
    Index();
    void import(const QString& docContent);
    void initTrie();
    QList<Doc> queryAnd(const QStringList& words) const;
    QList<Doc> expansionQueryAnd(const QStringList& words) const;
    void load(QFile &file);
    void save(QFile &file);
    qint64 getDocCount() const;
};

/**
 * Imports any container to the index.
 */
template<typename Container>
void importToIndex(Index &index, Container &c)
{
    for (auto i = c.begin(); i != c.end(); i++)
    {
        index.import(*i);
    }
}

#endif // INDEX_H
