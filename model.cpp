#include "model.h"

#include <exception>
#include <typeinfo>
#include <QDebug>
#include <QTime>

Model::Model() : m_indexLocation("index.index")
{
}

bool Model::init()
{
    QFile f(m_indexLocation);
    try
    {
        QTime stopWatch;
        stopWatch.start();
        m_index.load(f);
        int elapsedLoad = stopWatch.elapsed();
        m_index.initTrie();
        int elapsed = stopWatch.elapsed();
        qDebug() << "Index contains" << m_index.getDocCount() << "docs, loaded in" << elapsed << "ms.";
        qDebug() << "Pure load" << elapsedLoad << "ms";
        return true;
    }
    catch (const std::exception & ex)
    {
        qDebug() << typeid(ex).name() << ": " << ex.what();
    }

    return false;
}

void Model::setIndexLocation(const QString &location)
{
    m_indexLocation = location;
}

Index &Model::getIndex()
{
    return m_index;
}
