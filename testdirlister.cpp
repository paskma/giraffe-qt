#include "testdirlister.h"
#include "dirlister.h"
#include <QLinkedList>
#include <QString>
#include <iostream>
using namespace std;

TestDirLister::TestDirLister()
{

}

void TestDirLister::listTemp()
{
    const string dir("/tmp");
    cout << "Listing " << dir << endl;
    DirLister lister(dir.c_str());
    auto list = lister.allFiles();
    for (auto file : list)
    {
        cout << file.toStdString() << " ";
    }
    cout << endl;

    cout << "Returned files: " << list.size() << endl;
}
