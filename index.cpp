#include "index.h"

#include <QDataStream>
#include <QDebug>
#include <exception>

Index::Index()
{
}

void Index::addDocToIndex(const Doc& doc)
{
    QStringList words = doc.getWords();
    for (auto it = words.begin(); it != words.end(); it++)
    {
        QSet<qint64> &setOfIds = m_wordsToDocs[*it];
        setOfIds.insert(doc.getId());
    }
}

void Index::initTrie()
{
    for (auto it = m_wordsToDocs.begin(); it != m_wordsToDocs.end(); ++it)
    {
        m_wordTrie.add(it.key()) = it.key();
    }
    qDebug() << "Trie nodes" << m_wordTrie.nodeCount();
}

QDataStream & operator<<(QDataStream &stream, const Index &index)
{
    stream << index.m_docs;
    stream << index.m_lastUsedDocId;
    stream << index.m_wordsToDocs;
    return stream;
}

QDataStream & operator>>(QDataStream &stream, Index &index)
{
    stream >> index.m_docs;
    stream >> index.m_lastUsedDocId;
    stream >> index.m_wordsToDocs;
    return stream;
}

void Index::import(const QString& docContent)
{
    Doc doc(++m_lastUsedDocId, docContent);
    addDocToIndex(doc);
    m_docs.append(doc);
}

QList<Doc> Index::queryAnd(const QStringList &words) const
{
    QList<Doc> result;
    QSet<qint64> ids;
    bool firstRun = true;

    for (auto word : words)
    {
        QSet<qint64> next = m_wordsToDocs.value(Doc::normalizeWord(word));

        if (firstRun)
        {
            ids = next;
            firstRun = false;
        }
        else
        {
            ids.intersect(next);
        }

    }

    for (auto docId : ids)
    {
        result << m_docs[docId];
    }

    return result;
}

QSet<qint64> Index::wordExpansion(const QString& word) const
{
    /*
     * Less aggresive:
    const auto& nonexpanded = m_wordTrie.find(word);
    if (nonexpanded != "") {
        return m_wordsToDocs.value(word);
    }
    */

    QSet<qint64> result;
    std::vector<const QString*> expanded = m_wordTrie.findAll(word);
    for (auto i : expanded) {
        auto ids = m_wordsToDocs.value(*i);
        for (auto id : ids) {
            result.insert(id);
        }
    }
    return result;
}


QList<Doc> Index::expansionQueryAnd(const QStringList &words) const
{
    QList<Doc> result;
    QSet<qint64> ids;
    bool firstRun = true;

    for (auto word : words)
    {
        QSet<qint64> next = wordExpansion(Doc::normalizeWord(word));

        if (firstRun)
        {
            ids = next;
            firstRun = false;
        }
        else
        {
            ids.intersect(next);
        }

    }

    for (auto docId : ids)
    {
        result << m_docs[docId];
    }

    return result;
}

void Index::load(QFile &file)
{
    bool ret = file.open(QFile::ReadOnly);
    if (!ret) {
        IndexException ex(QString("Error loading index from ") + file.fileName());
        throw ex;
    }
    QDataStream in(&file);
    in >> (*this);
    file.close();
}


void Index::save(QFile &file)
{
    bool ret = file.open(QFile::WriteOnly);
    if (!ret)
        throw IndexException(QString("Error saving index to ") + file.fileName());
    QDataStream out(&file);
    out << (*this);
    file.close();
}

qint64 Index::getDocCount() const
{
    return m_lastUsedDocId + 1;
}


IndexException::IndexException(const std::string &a_msg) : msg(a_msg)
{
}

IndexException::IndexException(const QString &a_msg) : msg(a_msg.toStdString())
{
}

const char *IndexException::what() const noexcept
{
    return msg.c_str();
}


