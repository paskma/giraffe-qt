CONFIG += qtestlib

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    ../testqstring.cpp \
    ../testdirlister.cpp \
    ../dirlister.cpp \
    ../testindex.cpp \
    ../testtrie.cpp \
    ../doc.cpp \
    ../index.cpp \
    ../trie.cpp \
    runtest.cpp

HEADERS += \
    ../testqstring.h \
    ../testdirlister.h \
    ../dirlister.h \
    ../testtrie.h \
    ../doc.h \
    ../index.h \
    ../trie.h \
    ../testindex.h
