#include <QCoreApplication>
#include <QtTest/QtTest>
#include <iostream>

#include "../testqstring.h"
#include "../testdirlister.h"
#include "../testindex.h"
#include "../testtrie.h"

using namespace std;

int main(int argc, char *argv[])
{
    cout << "Runtest started." << endl;
    QCoreApplication app(argc, argv);
    
    TestQString tc; 
    QTest::qExec(&tc, argc, argv);

    TestDirLister testDirLister;
    QTest::qExec(&testDirLister, argc, argv);
    
    TestIndex testIndex;
    QTest::qExec(&testIndex, argc, argv);

    TestTrie testTrie;
    QTest::qExec(&testTrie, argc, argv);

    return 0;
}
