#ifndef DIRLISTER_H
#define DIRLISTER_H

#include <QObject>
#include <QLinkedList>

class DirLister : public QObject
{
    Q_OBJECT
private:
    QString m_directory;
public:
    explicit DirLister(const QString & directory);
    QLinkedList<QString> allFiles();
    
signals:
    
public slots:
    
};

#endif // DIRLISTER_H
