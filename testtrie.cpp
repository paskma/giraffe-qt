#include "testtrie.h"
#include "trie.h"
#include <QtTest/QtTest>

TestTrie::TestTrie()
{

}

void TestTrie::emptyTrie()
{
    Trie<std::string, int> t;
    QVERIFY(t.nodeCount() == 1);
}

void TestTrie::oneChar()
{
    Trie<std::string, int> t;
    t.add("a") = 100;
    QVERIFY(t.nodeCount() == 2);
}

void TestTrie::lookup()
{
    Trie<std::string, int> t;
    t.add("ab") = 100;
    QVERIFY(t.nodeCount() == 3);
    QVERIFY(t.find("") == 0);
    QVERIFY(t.find("a") == 0);
    QVERIFY(t.find("ab") == 100);
    QVERIFY(t.find("abc") == 0);
}

void TestTrie::findAll()
{
    Trie<std::string, int> t;
    t.add("ab") = 100;
    const auto& a = t.findAll("a");
    QCOMPARE(a.size(), 1ul);
    QCOMPARE(*a[0], 100);

    const auto& ab = t.findAll("ab");
    QVERIFY(ab.size() == 1);
    QVERIFY(*ab[0] == 100);

    auto abc = t.findAll("abc");
    QVERIFY(abc.size() == 0);
}

void TestTrie::findAll2()
{
    Trie<std::string, int> t;
    t.add("aaaaaab") = 100;
    auto a = t.findAll("a");
    QVERIFY(a.size() == 1);
    QVERIFY(*a[0] == 100);
}
