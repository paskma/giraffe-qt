#ifndef DOC_H
#define DOC_H
#include <QString>
#include <QDataStream>
#include <QStringList>


class Doc
{
private:
    qint64 m_id;
    QString m_content;
    friend QDataStream & operator<< (QDataStream& stream, const Doc& doc);
    friend QDataStream & operator>> (QDataStream& stream, Doc& doc);
public:
    Doc();
    static QString normalizeWord(const QString &word);
    Doc(qint64 id, const QString & content);
    const QString & getContent() const;
    qint64 getId() const;
    bool operator==(Doc& doc);
    QStringList getWords() const;

    static bool compareByContent(const Doc &d1, const Doc &d2)
    {
        return d1.getContent() < d2.getContent();
    }
};



#endif // DOC_H
