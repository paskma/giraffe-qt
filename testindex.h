#ifndef TESTINDEX_H
#define TESTINDEX_H

#include <QObject>

class TestIndex : public QObject
{
    Q_OBJECT
public:
    explicit TestIndex(QObject *parent = 0);
    
signals:
    
private slots:
    void docSerialization();
    void docToWords();
    void indexQueryAnd();
    void indexSerialization();
    
};

#endif // TESTINDEX_H
