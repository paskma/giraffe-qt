#include "testqstring.h"
#include <QtTest/QtTest>

void TestQString::toUpper()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("HELLO"));
}

