#include "dirlister.h"

#include <QDirIterator>
#include <QString>
//#include <iostream>

DirLister::DirLister(const QString & directory)
{
    m_directory = directory;
}

QLinkedList<QString> DirLister::allFiles()
{
    QLinkedList<QString> result;
    QDirIterator iterator(m_directory,
        QDir::Dirs | QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

    while (iterator.hasNext())
    {
        QString file = iterator.next();
        //std::cout << file.toStdString() << std::endl;
        result << file;
    }

    return result;
}
