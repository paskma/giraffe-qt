#include "testindex.h"
#include "doc.h"
#include "index.h"

#include <QDebug>
#include <QtTest/QtTest>
#include <QTemporaryFile>

TestIndex::TestIndex(QObject *parent) :
    QObject(parent)
{
}

void TestIndex::docSerialization()
{
    QTemporaryFile file;
    QVERIFY(file.open());
    QDataStream out(&file);
    Doc doc1(2, "foo");
    out << doc1;
    file.flush();
    file.close();

    Doc doc2;
    QVERIFY(file.open());
    QDataStream in(&file);
    in >> doc2;
    QVERIFY(doc1 == doc2);
}

void TestIndex::docToWords()
{
    Doc d(1, "a b   c");
    auto words = d.getWords();
    QVERIFY(words[0] == "a");
    QVERIFY(words[1] == "b");
    QVERIFY(words[2] == "c");

    QString str1("ahoj/vole/blekota jekota&aa+bb,ccc-ddd__eee.fff=ggg[hhh](iii)jjj'kkk\"LLL");
    Doc docStr1(1, str1);
    // for (auto i : docStr1.getWords()) qDebug() << i;
    QString expected("ahoj vole blekota jekota aa bb ccc ddd eee fff ggg hhh iii jjj kkk lll");
    QString actual = docStr1.getWords().join(" ");
    QVERIFY(expected == actual);
}

void TestIndex::indexQueryAnd()
{
    Index index;
    QLinkedList<QString> rawDocs;
    rawDocs << "a b c" << "a b a" << "d e f";
    importToIndex(index, rawDocs);
    QStringList query = {"a", "b"};
    QList<Doc> result = index.queryAnd(query);
    QVERIFY(result.size() == 2);
    qSort(result.begin(), result.end(), Doc::compareByContent);
    //qDebug() << result.at(0).getContent();
    //qDebug() << result.at(1).getContent();
    QVERIFY(result.at(0).getContent() == "a b a");
    QVERIFY(result.at(1).getContent() == "a b c");
}

void TestIndex::indexSerialization()
{
    Index index;
    QList<QString> rawDocs = {"a b c"};
    importToIndex(index, rawDocs);

    QTemporaryFile file;
    index.save(file);

    Index index2;
    index2.load(file);

    QStringList query = {"a"};
    QList<Doc> result = index2.queryAnd(query);
    QVERIFY(result.size() == 1);
    QVERIFY(result.at(0).getContent() == "a b c");
}
