#include <QApplication>
#include <iostream>

#include "mainwindow.h"
#include "buildindex.h"
#include "model.h"

using namespace std;

int guiMain(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Model model;
    MainWindow mainWindow(model);
    model.init();
    mainWindow.show();
    return a.exec();
}

int consoleMain(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    cout << "Arguments: " << a.arguments().join(" ").toStdString() << endl;
    QStringList dirs, arguments = a.arguments();
    for (int i = 1; i < arguments.size(); i++)
        dirs << arguments.at(i);

    BuildIndex builder;
    builder.build(dirs, "index.index");

    cout << "Done." << endl;
    return 0;
}

//#define PROFILE

#ifndef PROFILE
int main(int argc, char *argv[])
{
    cout << "Started." << endl;
    if (argc > 1)
        return consoleMain(argc, argv);
    else
        return guiMain(argc, argv);
}
#else
int main()
{
    char *argv[2];
    argv[0] = (char*)"thebinary";
    argv[1] = (char*)"/lvdata/media/mp3";
    int argc = 2;
    return consoleMain(argc, argv);
}
#endif
