#ifndef TESTDIRLISTER_H
#define TESTDIRLISTER_H

#include <QObject>


class TestDirLister: public QObject
{
    Q_OBJECT
private slots:
    void listTemp();
public:
    TestDirLister();
};

#endif // TESTDIRLISTER_H
